#!/usr/bin/env bash
which gitbook || npm install gitbook-cli -g
which book    || npm install gitbook-summary -g

cd ./WotP
book sm
cd ..
gitbook install && gitbook build . ./public
