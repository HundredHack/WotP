# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
+ Reorganized the Questing chapter, adding a subchapter for dangers and moving rules for major mental damage, exposure, suffocation, starvation, and burning into this subchapter.
  Also moved the rules for diseases and poisons into the danger subchapter, though each with their own page to accommodate lists of each.

### Added
+ Numerous diseases into the new danger subchapter of Questing.

## [0.9.0] - 2017-12-28
### Added
+ Passive trait to Knacks, allowing for additional knacks which do not have to be activated.
+ Prerequisites trait to Knacks, allowing for knacks which require the character to meet some condition(s) to acquire and improve.

## [0.8.0] - 2017-12-28
### Added
+ Starting Relationships to character generation, including an example.

## [0.7.1] - 2017-12-27
### Fixed
+ Replaced all curly quotes with straight single quotes.

## [0.7.0] - 2017-12-27
### Changed
+ Rules for the core mechanic.
  These changes separate characteristics from skills and make skills bonuses to tests whose base is determined by the appropriate characteristic.
  Further, tests are now broken into Active and Reactive tests - this gives a slight benefit to the character reacting to an attack / poison / pressure, but can be readily overcome by training in a relevant skill.
  This set of changes updates the language and rules across the book to reflect this change.

### Fixed
+ Removed erroneous mention of attractiveness from Charisma description.
+ Rules for Wealth at character generation.
  This change gives players a random starting wealth and rules for purchasing additional equipment at character creation which use the actual wealth skill.

## [0.6.1] - 2017-12-26
### Added
+ The tables for Knacks, Miracles, and Dweomers have been linkified and now take you to the appropriate description when clicked.

### Fixed
+ Ensured all chapter references link to the actual chapter instead of being placeholders.

## [0.6.0] - 2017-12-26
### Changed
+ Replaced the monolithic **Animals** table with a quickref table and individual pages for each animal.

### Fixed
+ Typo in the **Enhance Skill** Knack entry.
+ Typo in the title for the **Beasts** page.

## [0.5.0] - 2017-12-14
### Changed
+ Altered the rules for Knacks, namely:
  1. Ensured that the maximum magnitude of any given knack for a character is equal to 1/10th their Knackery score; this makes Knackery magnitude comparable to Complexity for Crafters.
  2. Changed the Draw Power rules so that each successive draw imposes a cumulative -25% penalty to the attempt.
  3. Removed the rules that implied characters need to roll to exercise knacks and made it clear that they just choose to exercise them.
  4. Removed the rules for reducing the cost of exercising knacks.
     Playtesting revealed that these rules slowed the game down for almost no benefit.
     This increases the value of higher-point Knacks.

## [0.4.1] - 2017-12-14
### Fixed
+ Formatting for multiplication, standardizing on the `times` symbol.
+ Formatting for dice rolls, standardizing across the rules.
+ Ensured all distances are stated in yards rather than a mix of yards and meters.

## [0.4.0] - 2017-12-14
### Added
+ Added JSON files representing cards for dweomers, knacks, and miracles.
  The files can be imported into [RPG-Cards](http://crobi.github.io/rpg-cards/) and printed from there.

## [0.3.3] - 2017-12-14
### Fixed
+ Fixed formatting for Combat Actions table.

## [0.3.2] - 2017-12-14
### Fixed
+ Fixed formatting for Major Wounds table.

## [0.3.1] - 2017-12-14
### Fixed
+ Replaced references to OpenQuest with references to Tephra/WotP.

## [0.3.0] - 2017-12-14
### Fixed
+ Filled the blank list of character generation steps and linked them to the subchapters.

## [0.3.0] - 2017-12-14
### Added
+ Rules for improving skills through use.

## [0.2.3] - 2017-12-13
### Added
+ Added major mental damage rules.

## [0.2.1] - 2017-12-13
### Fixed
+ Fix erroneous characteristics definition in Streetwise skill.

## [0.2.0] - 2017-12-13
### Added
+ Consistent rule for combat damage bonus.

### Changed
+ Beasts and Animals to include consistent damage bonus based on the rule.

## [0.1.0] - 2017-12-11
### Added
+ Initial documents, a _very_ rough translation from standard OpenQuest to the world of Tephra.
  This release is riddled with issues and needs a lot of clarification and work.