# Poison
Plants and creatures have developed poisons as a method of protecting themselves against predators.
They are also used by assassins and wrong doers of all kinds to murder their victims.


Every type of poison has the following information detailed:
+ Name: The poison's name.
  Also, if the poison is magical in nature, it will be mentioned here.
+ Delay: The time between the poison's introduction to a character, to the time its effect takes hold.
+ Duration: How long the poison, if effective, will affect the victim.
  The effects of the poison cannot be removed or healed until the poison itself has been neutralised or has dissipated in the victim's system.
  Damage caused by poison will not automatically heal – it must be healed through magical or natural healing.
  Some poisons have a duration of permanent because their effects on a victim are additive and do not filter out of the body over time.
+ Potency: This is a number between 10 and 100 that measures the strength of a poison.
  Some poisons, like Basilisk Venom, have even higher Potencies.
  A character must make an opposed Resilience test versus the poison's Potency test in order to avoid or mitigate the damage of the poison.
+ Effect: Usually hit point damage, though this is not universal.
  Some poisons cause a character to sleep for a period of time.
  More exotic poisons may cause hallucinogenic effects, paralysis or a combination of effects.
  These take place after the delay noted above.

### Poison Succeeds, Character Fails
If the poison succeeds its Potency test and the character fails his Resilience test, the poison has its full effect.
### Character Succeeds, Poison Fails
If the character succeeds his Resilience test and the poison fails its Potency test, the poison has no effect.
### Both Poison and Character Succeed
Whoever rolled the highest in their test wins.
### Both Poison and Character Fail
Whoever rolled the lowest in their test wins.

## Poisons List
The following list of diseases is _non-exhaustive_.

| Poison                    | Delay                   | Duration                | Potency  | Effect |
|:--------------------------|:-----------------------:|:-----------------------:|:--------:|:-------|
| Belladonna                | $$1\text{D}4$$ minutes  | $$4\text{D}6$$ hours    | $$40\%$$ | $$2\text{D}4$$ damage / hour, $$-2$$ REA
| Curare                    | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$35\%$$ | Paralysis and suffocation
| Dumb Cane                 | $$1\text{D}4$$ rounds   | $$1\text{D}6$$ minutes  | $$40\%$$ | Unable to speak
| Hemlock                   | $$5\text{D}12$$ minutes | $$1\text{D}4$$ hours    | $$65\%$$ | Paralysis and suffocation
| Metal, Arsenic            | $$5\text{D}12$$ minutes | Permanent               | $$65\%$$ | $$-2\text{D}4$$ BOD
| Metal, Lead               | $$3\text{D}4$$ minutes  | Permanent               | $$55\%$$ | $$-1\text{D}4$$ REA
| Metal, Mercury            | $$2\text{D}6$$ days     | Permanent               | $$60\%$$ | $$-2$$ BOD, DEX, and REA
| Mushroom, Deadly Galerina | $$5\text{D}12$$ minutes | $$2\text{D}6$$ hours    | $$30\%$$ | Fatigued, $$-1\text{D}4$$ BOD
| Mushroom, Deathcap        | $$3\text{D}4$$ hours    | $$2\text{D}4$$ days     | $$80\%$$ | $$2\text{D}6$$ damage / day, $$-2\text{D}4$$ BOD
| Mushroom, Jack O Lantern  | $$5\text{D}12$$ minutes | $$3\text{D}8$$ hours    | $$15\%$$ | Fatigued
| Opium                     | $$1\text{D}4$$ rounds   | $$1\text{D}4$$ hours    | $$40\%$$ | $$-1$$ INT, REA, and CHA
| Ouabain                   | $$1\text{D}4$$ rounds   | $$2\text{D}6$$ minutes  | $$45\%$$ | $$2\text{D}4$$ damage / minute
| Strychnine                | $$2\text{D}4$$ minutes  | $$2\text{D}12$$ hours   | $$55\%$$ | $$2\text{D}4$$ damage / hour, $$-1$$ BOD and DEX
| Toxin, Beetle             | $$1\text{D}4$$ rounds   | $$1\text{D}4$$ days     | $$50\%$$ | $$2\text{D}4$$ BOD
| Toxin, Bird               | $$1\text{D}4$$ rounds   | Permanent               | $$65\%$$ | $$3\text{D}4$$ damage, $$-2\text{D}4$$ BOD and DEX
| Toxin, Fish               | $$1\text{D}4$$ rounds   | $$3\text{D}8$$ hours    | $$65\%$$ | Paralysis and suffocation
| Toxin, Frog               | $$1\text{D}4$$ rounds   | Permanent               | $$65\%$$ | $$3\text{D}4$$ damage, $$-2\text{D}4$$ BOD and DEX
| Toxin, Gascon             | $$1\text{D}4$$ rounds   | $$5\text{D}6$$ minutes  | $$40\%$$ | All tests are hard
| Venom, Giant Spider       | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$25\%$$ | $$2\text{D}4$$ damage, $$-2$$ DEX
| Venom, Giant Wasp         | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$15\%$$ | $$1\text{D}4$$ damage
| Venom, Gorgon Serpent     | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$35\%$$ | $$1\text{D}4$$ damage, $$-3$$ BOD
| Venom, Octopus            | $$1\text{D}4$$ rounds   | $$3\text{D}8$$ hours    | $$65\%$$ | Paralysis and suffocation
| Venom, Scorpion           | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$20\%$$ | $$1\text{D}6$$ damage / 10 minutes
| Venom, Viper              | $$1\text{D}4$$ rounds   | $$6\text{D}10$$ minutes | $$50\%$$ | $$1\text{D}4$$ damage / 10 minutes, $$-3$$ BOD
| Wolfsbane                 | $$1\text{D}4$$ rounds   | $$3\text{D}8$$ hours    | $$40\%$$ | $$2\text{D}6$$ damage / hour, $$-1\text{D}4$$ BOD


<!-- Block-Breaking Comment to allow adjacent alert blocks -->
<!--
> **[danger] Poison**
>
> _Description_
> + **Delay:**
> + **Duration:**
> + **Potency:**
> + **Effect:**
-->

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Belladonna**
>
> _The leaves and berries of this nightshade plant are often used for cosmetic, anesthetic,  and recreational uses._
> _However, if ingested in sufficient quantities, this substance causes an onset of blurred vission, loss of balance, slurred speech and light sensitivity followed by delirium, hallucinations, and convulsions._
> + **Delay:** $$1\text{D}4$$ minutes
> + **Duration:** $$4\text{D}6$$ hours
> + **Potency:**  $$40\%$$
> + **Effect:** $$1\text{D}4$$ damage every hour, $$-2$$ REA

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Curare**
>
> _This poison is often used on arrowheads and javelins and is used to paralyze and kill via asphyxiation as the victim's respiratory system is unable to contract._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$6\text{D}10$$ minutes
> + **Potency:** $$35\%$$
> + **Effect:** Victim becomes paralyzed and can no longer breathe, beginning to suffocate once they are no longer able to hold their breath.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Dumb Cane**
>
> _Though not lethal, this poison can be radically effective for preventing someone from speaking, causing their mouth to numb, drool, and swell._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$1\text{D}6$$ minutes
> + **Potency:** $$40\%$$
> + **Effect:** Victim is unable to speak for the duration.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Hemlock**
>
> _This poison, made from a tall, hardy plant's roots, causes the victim's muscles to weaken and inflicts paralysis on them, causing them to suffocate._
> + **Delay:** $$5\text{D}12$$ minutes
> + **Duration:**  $$1\text{D}4$$ hours
> + **Potency:** $$65\%$$
> + **Effect:** Victim becomes paralyzed and can no longer breathe, beginning to suffocate once they are no longer able to hold their breath.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Metal, Arsenic**
>
> _Ingestion of lethal doses of arsenic causes the victim's organs to fail and includes a particular garlic odor of both breath and body._
> + **Delay:** $$5\text{D}12$$ minutes
> + **Duration:** Permanent
> + **Potency:** $$65\%$$
> + **Effect:** $$-2\text{D}4$$ BOD

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Metal, Lead**
>
> _Acute poisoning of lead causes the victim to vomit, grow weak, seizures, delerium, weight loss, and in general destroying the mind and the body._
> + **Delay:** $$3\text{D}4$$ minutes
> + **Duration:** Permanent
> + **Potency:** $$55\%$$
> + **Effect:** $$-1\text{D}4$$ REA

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Metal, Mercury**
>
> _Acute exposure to mercury degrades the victim's body and mind, causing shakes, a breakdown in their nervous system, and vomiting._
> + **Delay:** $$2\text{D}6$$ days
> + **Duration:** Permanent
> + **Potency:** $$60\%$$
> + **Effect:** $$-2$$ BOD, DEX, and REA

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Mushroom, Deadly Galerina**
>
> _Poisoning by this mushroom takes time; first, the victim feels a sense of unease which rapidly progresses to vomiting, violent cramps, and diarrhea._
> _Finally, the liver and kidneys shut down and death soon follows._
> + **Delay:** $$5\text{D}12$$ minutes
> + **Duration:** $$2\text{D}6$$ hours
> + **Potency:** $$30\%$$
> + **Effect:** The victim is Fatigued for the duration, $$-1\text{D}4$$ BOD

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Mushroom, Deathcap**
>
> _Ingesting this pleasant-tasting mushroom almost always kills the victim, though they won't know it for several hours._
> _Once the symptoms begin to show, causing diarrhea, nausea, and vomiting, it is too late - the effects quickly progress to delerium, seizures, and coma as the kidneys and liver fail completely._ 
> + **Delay:** $$3\text{D}4$$ hours
> + **Duration:** $$2\text{D}4$$ days
> + **Potency:** $$80\%$$
> + **Effect:** $$2\text{D}6$$ damage every day, $$-2\text{D}4$$ BOD

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Mushroom, Jack O Lantern**
>
> _Ingesting this mushroom, while not lethal, causes the victim to suffer from severe cramps, vomiting, and diarrhea._
> + **Delay:** $$5\text{D}12$$ minutes
> + **Duration:** $$3\text{D}8$$ hours
> + **Potency:** $$15\%$$
> + **Effect:** The victim is fatigued for the duration.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Opium**
>
> _Though used as a medication and recreational drug, this substance can sunder the victim's consciousness and depress their body's responses, often leading to respiratory failure and death._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$1\text{D}4$$ hours
> + **Potency:** $$40\%$$
> + **Effect:** $$-1$$ INT, REA, and CHA

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Ouabain**
>
> _This poison made from the boiled leaves and branches of toxic plants causes the victim to endure muscle spasms, trouble breathing, and in fatal cases, cardiac arrest._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$2\text{D}6$$ minutes
> + **Potency:** $$45\%$$
> + **Effect:** $$2\text{D}4$$ damage every minute.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Strychnine**
>
> _This relatively fast acting poison wracks the victim with terrible convulsions which eventually spread to their respiratory system._
> + **Delay:** $$2\text{D}4$$ minutes
> + **Duration:** $$2\text{D}12$$ hours
> + **Potency:** $$55\%$$
> + **Effect:** $$2\text{D}4$$ damage every hour, $$-1$$ BOD and DEX

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Toxin, Beetle**
>
> _This odorless, colorless poison is made from the crushed bodies of poisonous beetles causes the skin to welt and blister on contact; ingestion has a similar effect on internal organs._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$1\text{D}4$$ days
> + **Potency:** $$50\%$$
> + **Effect:** $$2\text{D}4$$ BOD

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Toxin, Bird**
>
> _Certain species of bird exude a neurotoxin through their skin and feathers which numbs on contact and damages internal organs severely if it gets into the bloodstream._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** Permanent
> + **Potency:** $$65\%$$
> + **Effect:** $$3\text{D}4$$ damage, $$-2\text{D}4$$ BOD and DEX

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Toxin, Fish**
>
> _Ingesting the poisonous flesh of the puffer fish causes the victim to go numb as their nervous system shuts down, reducing their ability to respond and eventually causing them to cease breathing._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$3\text{D}8$$ hours
> + **Potency:** $$65\%$$
> + **Effect:** Victim becomes paralyzed and can no longer breathe, beginning to suffocate once they are no longer able to hold their breath.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Toxin, Frog**
>
> _The nerotoxins of some frog species cause permanent damage to the victim, destroying their nervous system in moments._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** Permanent
> + **Potency:** $$65\%$$
> + **Effect:** $$3\text{D}4$$ damage, $$-2\text{D}4$$ BOD and DEX

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Toxin, Gascon**
>
> _The saliva of some Gascons is a potent drug and poison for other peoples, causing them to feel incredibly drunk for a few minutes._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$5\text{D}6$$ minutes
> + **Potency:** $$40\%$$
> + **Effect:** All tests are hard for the duration.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Venom, Giant Wasp**
>
> _The stings of these terrifying flying monstrosities are incredibly painful; repeated stings are frequently fatal._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$6\text{D}10$$ minutes
> + **Potency:** $$15\%$$
> + **Effect:** $$1\text{D}4$$ damage

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Venom, Octopus**
>
> _The toxins secreted by some octopi are weaponized by the creatures and cause numbing and paralysis, including of the respiratory system, on contact._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$3\text{D}8$$ hours
> + **Potency:** $$65\%$$
> + **Effect:** Victim becomes paralyzed and can no longer breathe, beginning to suffocate once they are no longer able to hold their breath.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Venom, Scorpion**
>
> _The painful sting from this scorpion spreads quickly through the victim's system._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$6\text{D}10$$ minutes
> + **Potency:** $$20\%$$
> + **Effect:** $$1\text{D}6$$ damage every ten minutes.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Venom, Viper**
>
> _The venom of this terrible viper causes severe pain and prevents the blood from coagulating while dissolving muscle tissue at the bite location._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$6\text{D}10$$ minutes
> + **Potency:** $$50\%$$
> + **Effect:** $$1\text{D}4$$ damage every ten minutes, $$-3$$ BOD

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Wolfsbane**
>
> _When poisoned by this plant, the victim's body tingles, they begin to feel nauseous and their heartbeat becomes irregular._
> _As time goes on, the poisoning will lead to death as multiple organs begin to fail, including the heart._
> + **Delay:** $$1\text{D}4$$ rounds
> + **Duration:** $$3\text{D}8$$ hours
> + **Potency:** $$40\%$$
> + **Effect:** $$1\text{D}6$$ damage every hour, $$-1\text{D}4$$ BOD