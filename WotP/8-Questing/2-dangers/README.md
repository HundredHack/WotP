# Dangers
Questing in Tephra is not a safe and carefree endeavor.
Many, many bad things may befall characters who go questing, even when not taking outright combat into account.

## Major Mental Damage
When characters witness horrific events, the Games Master will ask the players to make a Persistence test.
Should that fail, then the character has suffered a mental blow so severe their persona has become altered by it.
Roll on the Mental Damage Table below to see what kind of effect that the character suffers from..  

Independent of whether the Persistence test was successful, they must immediately make a hard Resilience, or go into shock for $$1\text{D}4$$ rounds. 

| Roll $$1\text{D}6$$ | Effect |
|:--------:|:-------|
| 1        | The shakes – the incident has left you with an uncontrollable but slight and permanent jittery shake.  Lose 2 DEX. 
| 2        | Dislocation – you find it hard to connect with people, it seems easier to remain unfeeling, to simply let things wash right over you.  CHA is reduced by 2. 
| 3        | Rage – suddenly everything and everyone around you is a constant sign of irritation.  This irritation you find is best expressed through physical violence.  Each time such a situation arises, you must make a Persistence test.  Pass and you've controlled your rage, fail and you have no recourse but to lash out – either with your fists or any weapons you are carrying.   
| 4        | Bottling it – when finding yourself in dangerous and stressful situations you have an overwhelming urge to flee, to find safety.  Each time such a situation arises, you must make a Persistence test.  Pass and you've controlled your urge to run, fail and you've bottled it totally.  
| 5        | Nightmares – every night they invade your dreams, forcing you to relive over and over again the things you have witnessed.  Sleep becomes almost impossible, a curse rather than a blessing.  BOD is reduced by 2.    
| 6        | Focus – You find yourself having difficulty focusing on the task in hand.  Just keeping aware is a struggle.  Both CHA and INT are reduced by 2.

## Exposure, Starvation and Thirst
A character can normally survive for a number of hours equal to his BOD + CHA before suffering from exposure.

A character can survive for a number of days equal to his BOD + CHA before becoming starved, though after three days they will begin to suffer a –25% penalty to Fatigue tests.

A character can survive for a number of hours equal to his $$(\text{BOD} + \text{CHA}) \times 2$$ before becoming chronically thirsty, though particularly arid environments may reduce this to $$(\text{BOD} + \text{CHA})$$ or even $$\frac{\text{BOD} + \text{CHA}}{2}$$
Whenever a character is suffering from exposure, starvation or thirst, the Fatigue test penalty immediately doubles to –20%.
In addition, the character will automatically suffer one $$\text{D}6$$ of damage every day, for every condition he is experiencing.
Natural or magical healing will not heal this damage – only sufficient shelter, food or water can remedy the problem and allow natural or magical healing to take place.

## Falling
A character that takes damage from a fall ends up prone.
Armour points do not reduce falling damage.

A character takes $$1\text{D}6$$ damage per full $$3\text{yd}$$ fallen.

As long as the character was not surprised, they may attempt an Athletics test to mitigate falling damage.
A successful test allows the character to treat the fall as if it were two yards shorter than it actually is.
In addition, as long as this test is a success and the character is not reduced to 0 hit points due to the fall, the character lands safely and is not prone.
If the roll is a critical then miraculously no damage is taken. If the roll is a fumble then the maximum possible damage is taken.

Characters falling onto soft surfaces may have the distance they fall effectively halved for the purposes of damage.

## Suffocation
While underwater or moving through a poison gas cloud a character can hold his breath for a number of Combat Rounds equal to his BOD.

Once a character has surpassed the time for which he can hold his breath, he must make a Resilience test every round with a cumulative –10% penalty.
If he fails, he automatically starts inhaling the suffocating substance.

Armour points do not reduce suffocation damage.
The damage will only cease once the character can draw breathable air once more.
Even then, the character will require a Resilience test to be able to do anything other than wretch or gasp for breath for $$1\text{D}4$$ Combat Rounds.

| Substance Inhaled | Damage |
|:-----------------:|:------:|
| Water             | $$2\text{D}6$$
| Vacuum            | $$2\text{D}6$$
| Thick Smoke       | $$1\text{D}6$$
| Poison Gas        | Character is exposed to the poison. If the gas is also a thick smoke, then $$1\text{D}6$$ damage is incurred in addition to the poison's effect.

## Burning
The amount of damage per Combat Round suffered from fire or heat will depend on its intensity, as shown on the table below.
Metal armour, such as Plate or scale mail, does not subtract from the rolled damage.

| Damage Source | Example                                       | Damage per Combat Round |
|:-------------:|:----------------------------------------------|:-----------------------:|
| Flame         | Candle                                        | 1                       |
| Large Flame   | Flaming brand                                 | $$1\text{D}4$$          |
| Small Fire    | Camp fire, cooking fire                       | $$1\text{D}6$$          |
| Large Fire    | Scolding steam, large bonfires, burning rooms | $$2\text{D}6$$          |
| Inferno       | Lava, inside a blast furnace                  | $$3\text{D}6$$          |