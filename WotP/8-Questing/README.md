# Questing

## Improving Characters

### Spending Improvement Points

#### Improving Skills

The first time a character succeeds on a normal or hard test under duress which uses a skill, place a mark beside the skill.
At the completion of a quest or when beginning an interlude - whenever there's a sensible break in the quest to reflect and take time to recover - players should roll 1d100 for each skill they marked during play.
If the result of the roll is _greater than_ the score of the skill they're rolling for, they have improved it.
In this case, roll $$1\text{D}4$$ and add it to the skill.
This is your new skill score.

_Every time_ you fumble a test using a skill, add $$1\text{D}4$$ to the skill.
This represents a character gleaning new understanding from failure. 

A player can choose to spend one improvement point to increase one known skill.
Select the skill to be increased and the skill increases by $$2\text{D}4$$ points.
There is no limit to the score a skill can reach.

#### Improving Characteristics

A player can choose to spend three improvement points to increase one Characteristic by one point.
The maximum a human character can increase a Characteristic to is 21.
For non-humans, the maximum for a Characteristic is equal to the maximum possible starting score for the Characteristic plus three.

> **[info] Summary of Improvement**
>
> | Cost           | Improvement              |
|:----------------:|:-------------------------|
| 1 IP             | $$+2\text{D}4$$ to skill |
| 3 IP             | +1 to any characteristic |
| 1 IP / Magnitude | Acquire a Knack          |
| 2 IP / Magnitude | Acquire a Miracle        |
| 2 IP             | Learn a Dweomer          |

### Improving Outside of Quests: Practice & Research

The characters may often experience long stretches of ‘downtime' between Quests.
This is quite normal.
Group members may need to heal from wounds suffered during the last Quest, the characters may engage in some activity that takes time, or life may simply return to normal until the next danger to face the player characters appears.

During such downtime the characters may improve their characters.
The players might actually request downtime between Quests to learn new skills and it is up to the Games Master to determine if this is appropriate.

For each three month period of practice or research a character may gain 1 Improvement Point.