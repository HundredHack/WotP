# Spot Rules

## Fatigue
Combat, sprinting, climbing, swimming against a strong current, are all examples of when a character can become fatigued and tired.

If the Games Master thinks that a character has been engaged in an activity that may have drained him of physical energy, then they may call for a Resilience roll.
If the character fails the roll they suffer the effects of Fatigue (see below).

This test is usually made after the activity has been completed, unless the activity is long and drawn out and there is a real danger that Fatigue will stop the task being completed successfully.
For example, on a long hard march the characters are pressing on ahead so that they can reach a fort before an enemy army arrives there.
In this case there is a real danger that they will arrive not only too late but tired and worn down.

### The Effects of Fatigue
If a character fails the Resilience test then they become fatigued.
All skill tests are at -25%.
Also movement rate drops by a quarter.

If the fatigued character insists on engaging in heavy activity, such as combat, heavy labour or running, then another Resilience roll is made at -25%.
If the character fails this second skill test they become heavily fatigued and all the above penalties are doubled.

If a character fumbles any of their Resilience rolls, then they immediately fall unconscious for $$3\text{D}6$$ minutes and upon waking are still fatigued.

### Recovering From Fatigue
A character who completely rests for 20 - BOD hours will remove the effects of any Fatigue.

A Vigour knack will also remove the effects of Fatigue.

## Healing
Healing can be performed in one of three ways – using the First Aid skill, a magical spell, or through natural healing, resting while the injuries heal themselves.

### Natural Healing
A character's Minor injuries regain BOD/4 (round up) hit point per 24 hours, as long as the character does not engage in anything more than light activity.

Natural healing does not help with Major Wounds.

A Major Wound requires treatment with a successful Healing test or magical healing.

Once this is done Major Wounds heal at a rate of one hit point per day, as long as the character does not engage in anything more than light activity, and the character succeeds a daily Resilience test.

### Magical Healing
However magical healing is achieved, whether from a spell, prayer or potion, it has an instantaneous effect.
It still requires a successful healing test, because no magic can cure a creature of wounds the character does not understand.

In addition to the restoration of hit points, any character suffering a Major Wound that receives even a single hit point restoration through magical healing will immediately stop bleeding and is healed sufficiently to benefit from natural healing.

If a character has been knocked unconscious due to a Major Wound, the restoration of a single hit point to the wound that caused the unconsciousness will revive the character.

Unless specifically stated, magical healing cannot re-attach severed limbs or revive the dead.

## Encumbrance
Every piece of equipment in the Equipment chapter has an Encumbrance (ENC) score, apart from those items that are too small or light.
Characters can usually ignore the effects on Encumbrance that these light items have until they start to carry a lot of them – assume that an average of 20 such items will equal 1 ENC, on the basis that the character has a suitable means of carrying them, such as a sack or backpack.

A character can carry equipment whose total ENC is less than or equal to his BOD without penalty.

Encumbrance is a measure of not only weight but also bulk of the item, reflecting the awkwardness of handling the item.

### Overloading
A character carrying total ENC greater than his BOD is Overloaded.

Overloaded characters suffer a –25% penalty to all tests that require physical actions, including combat tests and most tests that have DEX or BOD as a Characteristic.

Overloaded characters have their Movement halved.
They also suffer a –25% penalty to all Fatigue tests.

A character cannot carry more than twice his BOD in ENC.