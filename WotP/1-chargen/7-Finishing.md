# Step 7: Finishing Off

By this stage all that remains is to note down a few numbers on the character sheet and ponder about the character's background and motives.

## General Information
You can choose your characters own age or, optionally, roll $$2\text{D}6$$ and add $$16$$.

Ever character starts with two Shims.

> **[info] What Are Shims?**
>
> Shims are what distinguishes the player character from the normal folk in Tephra.
> Characters can spend a Shim to:
>
> + Re-roll any failed dice roll
> + Downgrade a Major Wound to a normal wound.
>   The character still takes the full damage they would normally to their Hit Points but do _not_ suffer the messy effects of a major wound.
> + Avoid character death.
>   If the character's Hit Points are reduced to zero or less, instead of dying the character is merely unconscious.
>   The character remains in such a state until the combat is over at which point they awaken with one Hit Point.
>
> Once Shims are spent they are gone.
> The GM awards Shims for moments of outstanding heroic play.

## Background, Appearance, and Personality
Even if you don't create a fully written up background it is worth making a mental note of what the character is like as a person and roughly what their background is previous to play, as well as visualizing what they actually look like.

A background is useful not only to give the character a history before the game starts but also as a way of noting all the ‘intangible' elements of the character's personality.

Writing down a background is optional, and can be done either before or after character generation, or can even emerge during play.

## Relationships
Relationships help tie the character into the world and give them hooks for quests and chances for character development.
Relationships, like skills, have a score - your starting relationships have a score of $$25\%$$.

All characters start with an ally and a dependent relationship and should choose one enemy _or_ organization for their final starting relationship.

For more information on Relationships, see [CH2](../2-tests-and-skills/3-Descriptions.md#relationships).

> **[success] Example: Kost's Relationships**
>
> Kost begins with three relationships: his Dependent relationship is to his students, his ally is Ganzorig, the Peace Chief, and his enemy is the Imperial Shadows.
>
> + **Ganzorig, Peace Chief of The Odod (Ally)**: $$25\%$$
> + **Kost's Students (Dependent)**: $$25\%$$
> + **The Imperial Shadows (Enemy)**: $$25\%$$

## Motives
Motives are what drive the characters' actions.
They are the character's goals, both short and long term. 

Long term motives are things that are life motives, and are only achievable over the period of a linked set of adventures, commonly known as a ‘campaign'.

> **[success] Example Long Term Motives:**
>
> + Become King of the Amber Lands.
> + Defeat the evil Sorcerer Zanab Khan.
> + Become the richest man in Red Hat County.
> + Avenge my Father's death.

Short term motives are usually relevant to the adventure currently being played, and are determined near the beginning of the session by the Players.

> **[success] Example Short Term Motives:**
>
> In the context of a royal monster hunt in the bleak ice deserts of Zhaind:
>
> + Kill the biggest monster on the hunt. 
> + Secure the rights to trade the hides of the monsters. 
> + Use the hunt to impress the King and improve their social standing at court. 
> + Map the ice deserts of Zhaind to increase the body of knowledge of the Royal Library. 

At the end of character generation choose two long term Motives for your character and at the beginning of each adventure choose a suitable short term motive.

Motives help you earn improvement points:

+ Every time a motive is brought into play in a concrete way, the character earns one improvement per session. 
+ Short term motives are removed at the end of the session, and if completed earn an additional two improvement points. 
+ When a long term motive is finally achieved it is removed from the player's character sheet and the character earns five improvement points.

## Wrapping Up
In summary:

+ Note down Shims and age. 
+ If you want to include a background narrative then do this now.
+ Select motives.
+ If you haven't done so already write the character's name on the character sheet.

Your character is ready to explore Tephra!