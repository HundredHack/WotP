# Step 6: Get Equipment

Each character typically starts off with some money and the tools of their trade.

+ Starting Wealth skill score is $$5\text{D}20\%$$
+ Each character starts out with one of the two packages below:
  1. Padded armor, ranged weapon, close combat 2H weapon, and dagger.
  2. Padded armor, shield, ranged weapon, close combat 1H weapon, and dagger.
+ Characters also start out with the following equipment: Backpack, rope, two weeks worth of traveling provisions, flint and tinder, waterskin.

Further information about equipment and game economics is given in [CH4, Equipment][CH4].

Your GM may allow you to buy additional equipment before the game starts from the character's starting wealth: 
+ If the items are within your wealth bracket you may simply select to have them.
+ If they are one level higher than your wealth bracket you may test for each.
  If your test is successful, you have the item; if you fail, you do not.
  In any case, these tests do not impose a penalty on your Wealth tests.
+ If the items are two levels higher than your wealth bracket you may make a hard test for each.
  If your test is successful, you have the item; if you fail, you do not.
  In any case, these tests do not impose a penalty on your Wealth tests.