# The Craft and Crafting
Crafting is an approach to magic that acknowledges that there are magical rules that govern the Universe and that by studying these rules a Crafter can manipulate reality to their will.

Practitioners of the Craft develop in one of two ways.

The majority are organised into schools of Crafting, which have their own books of dweomers and rules that they teach their apprentices.
Alternatively, there is a long tradition of crafters working in solitude, cut off from other Crafters and society at large, to focus purely on their magical activities.
Occasionally they take on an apprentice, to teach their art, or simply as a helping hand around the magical laboratory.

## Ranks of Crafters
There are three basic ranks of Crafters:

+ **Apprentice.** Students of the Craft who will only know a couple of dweomers, usually including Mystic Vision, at a base of 40%.
  As well as being taught the Craft, they are expected to spend 60% of their time working for their tutors, performing menial tasks in their magical laboratories, or other jobs that their masters consider beneath them.
+ **Journeyman.** Graduates of the schools of wizardry.
  They will know between five and ten dweomers, and will have a Crafting skill ranging from 50% to 90%.
  If a member of a school of crafting, they will be expected to spend 30% of their time performing duties for the school, such as teaching apprentices or recovering lost magical knowledge.
+ **Master.** Acknowledged masters of the Craft.
  They will have at least ten dweomers and a Crafting skill of 90%+.
  If a member of a school of Crafting, then they will serve on its ruling body and have the complete resources of the school at their command.
  In return, it is expected that they spend 90% of their time researching, teaching and performing missions on the school's behalf.

## Learning the Craft
Before a dweomer can be applied using Crafting, the following process must be followed:

The character must first learn the dweomer through research.
In order to learn a particular dweomer, the character must possess the dweomer in written form or be taught it by a teacher.
In game terms this means having access to a teacher who knows the dweomer or a book or scroll where it is written down.
The player then spends two Improvement Points and writes the dweomer down on their character sheet.

Each dweomer is governed by the Crafting magical skill.
This skill is automatically acquired at its basic score (REA) when the character is first created.
This skill may be improved normally though the use of Improvement Points and practice.
Even non-Sorcerors have this skill, at base, since magic knowledge is widespread and simple spells are often used by normal folk - and nearly everyone in the civilized world interacts with magic items on occasions.

Once the dweomer has been learned, the character will be ready to try casting it.

## Manipulation and Complexity
Dweomers have three basic effects which can be manipulated by the Crafter: Magnitude, Duration, and Range.
Doing so, however, increases the complexity of the dweomer, a measure of how hard it is to work out and apply the dweomer.

Each effect has a default value which the dweomer can be applied at, starting at Complexity 1.
The default value for the dweomer effects are listed in the Manipulation table below. 

The tens value of the character's Crafting skill determines the maximum complexity that they can handle on each of the manipulation types.   

For example: Omar the Magnificent with a Crafting skill of 80% can handle an additional 8 complexity on manipulating each of the dweomer's effects, in Magnitude, Duration and Range.
That's a manipulation of up to 8 levels for each effect, not 8 levels in total across all three effects. 

However, just because a Crafter _can_ apply complex dweomers doesn't mean _it's safe to do so_.
The total compexity of a dweomer is the amount of damage inflicted on a character if they fumble applying a dweomer.

| Complexity Cost | Magnitude | Duration   | Range       |
|:---------------:|:---------:|:----------:|:-----------:|
| 1 (Default)     | 1         | 5 Minutes  | 10 Yards    |
| +1              | 2         | 15 Minutes | 20 Yards    |
| +2              | 3         | 30 Minutes | 40 Yards    |
| +3              | 4         | 1 Hour     | 80 Yards    |
| +4              | 5         | 2 Hours    | 1 Furlong   |
| +5              | 6         | 4 Hours    | 2 Furlongs  |
| +6              | 7         | 12 Hours   | 5 Furlongs  |
| +7              | 8         | 1 Day      | 1 Mile      |
| +8              | 9         | 2 Days     | 2 Miles     |
| +9              | 10        | 5 Days     | 5 Miles     |
| +10             | 11        | 1 Week     | 10 Miles    |
| +11             | 12        | 2 Weeks    | 20 Miles    |
| +12             | 13        | 1 Month    | 50 Miles    |
| +13             | 14        | 2 Months   | 100 Miles   |
| +14             | 15        | 1 Season   | 200 Miles   |
| +15             | 16        | 2 Seasons  | 500 Miles   |
| +16             | 17        | 1 Year     | 1000 Miles  |
| +17             | 18        | 2 Years    | 2000 Miles  |
| +18             | 19        | 5 Years    | 5000 Miles  |
| +19             | 20        | 10 Years   | 10000 Miles |

## Applying Dweomers
A character must be able to gesture with their hands, and be able to chant, in order to apply a dweomer.
Whenever a dweomer is applied, there will always be a sight and sound that nearby creatures can detect, be it a flash of light, a crack of thunder, or a shimmering in the air.
The exact effects are up to the Games Master and Player to decide, but will automatically be detected by any creatures within ten times the Magnitude of the spell in yards.  

Dweomers can be applied in two ways: 

1. slowly, carefully, in a ritual, or 
2. off-the-cuff, making all the requisite calculations mentally.

In the first case, the character must make a Crafting test, taking one minute per point of complexity for the dweomer.
Multiple Crafters working together can sreduce the time to apply the dweomer, but any one Crafter failing their test will cause the dweomer to fail.
The complexity limit of the dweomer is dependent on the Crafter involved with the Dweomer with the highest Crafting skill.
Failure to apply the dweomer means that the effort was wasted and can be retried.
Applying a dweomer slowly and carefully, following safety protocols prevents terrible side effects, including the damage caused by failing or fumbling.

In the second case, the character must make a hard crafting test to apply the dweomer by making quick mental calculations and adjustments.
Failing the test indicates that the character was unable to correctly adjudicate the energy and manipulations correctly and has caused a misapplication - see the table below.
Fumbling the test causes the Crafter to take damage equal to the Complexity of the dweomer.
This damage cannot be reduced by AP.

| Roll $$1\text{D}8$$ | Misapplication Effect |
|:--------:|:----------------------|
| 1        | Fizzle. The dweomer's energy dissipates harmlessly around the Crafter.
| 2        | Burn. The dweomer burns through the Crafter, the magical energy misdirected. Crafter takes $$1\text{D}4$$ damage.
| 3        | Explosion. The energy of the dweomer goes off catastrophically, causing an explosion whose diameter is equal to the complexity of the attempted dweomer. The explosion inflicts damage equal to half the dweomer's complexity to everyone in the sphere.
| 4        | Blind. The dweomer does not go off, but the energy of the dweomer radiates as a brilliant light centered on the Crafter, blinding the Crafter and anyone who looks at them for the next Combat round.
| 5        | Overpower. The dweomer is applied but with more power than intended; double the magnitude of the dweomer but inflict the additional complexity cost as damage on the Crafter.
| 6        | Memory Loss. Applying the dweomer without appropraite safeguards causing the magic to burn the Crafter's mind, making the Crafter lose some of what they've learned about the Craft. The dweomer, however, applies as normal. Reduce the character's Crafting skill by a number of points equal to the magnitude of the dweomer.
| 7        | Weak. The dweomer is successfully applied, but without any of the intended manipulations.
| 8        | Delayed. The Crafter fails to apply the dweomer this round but may still apply it in the next round automatically and without an additional test.