# Tests and Skills
Characters take tests to get things done in the game, almost always using skills to improve their chances.
When a player describes their character's intent and approach for a given action or reaction and the character can succeed or fail **and** when failure matters, the GM will ask the player to make a test against the relevant characteristic with an applicable skill bonus to see if the character is successful.

This chapter describes when and how to make tests, how to modify tests depending upon the conditions they are made under, and how to judge tests where two characters are competing against each other.
Finally, a (non-exhaustive) list of skills used in the game is detailed.

Characters are considered Masters in their fields of expertise when they are rated above 100%.
How WotP manages these very high skills is explained later in this chapter.