# Beasts

## Basilisk
> **[danger] Basilisk:**
>
> > Born from the egg of a cockerel acted upon in an Alchemist's or witches cauldron, this magical monster is the product of foul Sorcery.
> > It is a large lizard with multicoloured scales.
> > Its baleful gaze can kill and its blood is poisonous and corrosive.
> > Basilisks are usually employed as guardians of their master's treasure.
>
> #### Characteristics:
> **BOD:** $$5$$ **DEX:** $$7$$ **INT:** $$9$$ **REA:** $$3$$ **CHA:** $$16$$
> #### Attributes:
> **HP:** $$10$$ **MW:** $$5$$ **PP:** $$16$$ **AP:** $$2$$ **MR:** $$15$$
> #### Resistances:
> **Dodge:** $$30\%$$ **Persistence:** $$50\%$$ **Resilience:** $$70\%$$
> #### Skills:
> **Athletics:** $$60\%$$ **Deception:** $$40\%$$ **Natural Lore:** $$40\%$$
> #### Combat:
> The basilisk can attack with both gaze and bite simultaneously in the same action.
> + **Ranged Combat:** $$100\%$$
>   + **Gaze:** Death, range 16 yards
> + **Close Combat:** $$30\%$$
>   + **Bite:** $$1\text{D}6 + 1 + \text{Poison}$$
>
> #### Magic:
> **Poison Blood & Venom**: Any non-magical weapon hitting the basilisk corrodes in the creature's blood, completely disintegrating after $$\text{D}4$$ rounds.
> The basilisk's poison and corrosive blood are magical effects, which lose their special properties a few minutes after leaving the basilisk's body, making it virtually impossible to use the creature as a source for making lethal compounds.
>
> > **[danger] Basilisk Venom**
> > + **Type:** Ingested or smeared
> > + **Delay:** Immediate
> > + **Potency:** 65
> > + **Full Effect:** $$1\text{D}4$$ Hit Point damage, applies –6 penalty to victim's CON
> > + **Duration:** $$6\text{D}10$$ minutes
>
> **Death Gaze:** A basilisk can kill with a glance.
> In combat the basilisk glares at a single opponent each round.
> If the basilisk overcomes the target in an opposed test of its Persistence against the target's Resilience, the target dies instantly.
> Using the gaze attack costs no Power Points, and the basilisk may attack normally in any round in which it uses the gaze attack.
> This attack penetrates magical defences as if it were a Magnitude 6 Knack.
> If the target successfully resists the gaze attack, he is unharmed, though he may certainly be targeted again.

## Griffin
> **[danger] Griffin**
>
> > With the body of a lion and the head of an eagle and two eagle wings, the mighty Griffin is associated with the nobility, who often hunt it for sport.
> > It lairs in the mountains and is often the lord of its terrain.
>
> #### Characteristics:
> **BOD:** $$25$$ **DEX:** $$22$$ **INT:** $$14$$ **REA:** $$6$$ **CHA:** $$10$$
> #### Attributes:
> **HP:** $$16$$ **MW:** $$8$$ **PP:** $$10$$ **AP:** $$3$$ **MR:** $$22 (\text{Running}) | 44 (\text{Flying})$$
> #### Resistances:
> **Dodge:** $$40\%$$ **Persistence:** $$80\%$$ **Resilience:** $$70\%$$
> #### Skills:
> **Athletics:** $$80\%$$ **Deception:** $$30\%$$ **Perception:** $$50\%$$ **Natural Lore:** $$60\%$$
> #### Combat:
> + **Close Combat:** $$70\%$$
>   + **Bite:** $$1\text{D}8 + 6$$
>   + **Claw:** $$2\text{D}6 + 6$$

## Hippogriff
> **[danger] Hippogriff**
>
> > With the head and wings of a hawk on a body of a horse, this strange beast is often used as flying cavalry by those cultures that learn how to tame and breed them.
> > In the wild it is a fierce predator that values horse meat above all.
>
> #### Characteristics:
> **BOD:** $$29$$ **DEX:** $$17$$ **INT:** $$11$$ **REA:** $$7$$ **CHA:** $$9$$
> #### Attributes:
> **HP:** $$19$$ **MW:** $$10$$ **PP:** $$9$$ **AP:** $$1$$ **MR:** $$17 (\text{Running}) | 34 (\text{Flying})$$
> #### Resistances:
> **Dodge:** $$40\%$$ **Persistence:** $$40\%$$ **Resilience:** $$50\%$$
> #### Skills:
> **Athletics:** $$50\%$$ **Deception:** $$10\%$$ **Perception:** $$60\%$$ **Natural Lore:** $$60\%$$
> #### Combat:
> A Hippogriff can make two attacks per combat round. Either two foreclaw attacks or a Foreclaw and a bite
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}8 + 7$$
>   + **Foreclaw:** $$2\text{D}6 + 7$$

## Manticore
> **[danger] Manticore:**
>
> > This monster has the face of a man, the body of lion and the tail of a scorpion.
> > It wishes nothing but ill will towards other races.
> > It skulks in the wilderness a lone predator feeding on sentient creatures unlucky enough to encounter it.
>
> #### Characteristics:
> **BOD:** $$22$$ **DEX:** $$11$$ **INT:** $$14$$ **REA:** $$11$$ **CHA:** $$9$$
> #### Attributes:
> **HP:** $$15$$ **MW:** $$8$$ **PP:** $$9$$ **AP:** $$3$$ **MR:** $$22$$
> #### Resistances:
> **Dodge:** $$25\%$$ **Persistence:** $$65\%$$ **Resilience:** $$45\%$$
> #### Skills:
> **Athletics:** $$25\%$$ **Deception:** $$50\%$$ **Perception:** $$40\%$$
> #### Combat:
> The basilisk can attack with both gaze and bite simultaneously in the same action.
> + **Close Combat:** $$75\%$$
>   + **Claw:** $$1\text{D}6 + 6$$
>   + **Gore:** $$1\text{D}8 + 6$$
>   + **Poison Sting:** $$1\text{D}6 + 6 + \text{Poison}$$
>
> > **[danger] Manticore poison**
> > + **Type:** Ingested
> > + **Delay:** $$1\text{D}3$$ Combat Rounds
> > + **Potency:** 50
> > + **Full Effect:** $$1\text{D}4$$ Hit Point damage, applies –3 penalty to victim's BOD
> > + **Duration:** $$5\text{D}10$$ minutes
>
> #### Magic:
> Only if someone has been stupid enough to teach it some.
> It will take to it like a duck to water, usually learning  at least 5 points of Magnitude of which ever approach.

## Sea Serpent
> **[danger] Sea Serpent**
>
> > These long serpentine sea monsters are distantly related to Dragons.
> > They lair in caves at the bottom of the sea and this is where they drag their victims to be devoured. 
>
> #### Characteristics:
> **BOD:** $$43$$ **DEX:** $$7$$ **INT:** $$9$$ **REA:** $$3$$ **CHA:** $$12$$
> #### Attributes:
> **HP:** $$28$$ **MW:** $$14$$ **PP:** $$12$$ **AP:** $$5$$ **MR:** $$0 (\text{Running}) | 21 (\text{Swimming})$$
> #### Resistances:
> **Dodge:** $$40\%$$ **Persistence:** $$40\%$$ **Resilience:** $$80\%$$
> #### Skills:
> **Athletics:** $$60\%$$ **Stealth:** $$25\%$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$2\text{D}6 + 11$$

## Wyvern
> **[danger] Wyvern:**
>
> > These giant slender green reptiles are akin to dragons but with no forelegs and animal intelligence. 
>
> #### Characteristics:
> **BOD:** $$24$$ **DEX:** $$13$$ **INT:** $$8$$ **REA:** $$7$$ **CHA:** $$9$$
> #### Attributes:
> **HP:** $$17$$ **MW:** $$9$$ **PP:** $$9$$ **AP:** $$5$$ **MR:** $$13 (\text{Running}) | 26 (\text{Swimming})$$
> #### Resistances:
> **Dodge:** $$50\%$$ **Persistence:** $$35\%$$ **Resilience:** $$50\%$$
> #### Skills:
> **Athletics:** $$50\%$$ **Deception:** $$10\%$$ **Perception:** $$60\%$$
> #### Combat:
> In one combat round the Wyvern can use all three attacks. 
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}10 + 6$$
>   + **Sting:** $$1\text{D}5 + 6 + \text{Posion}$$
>   + **Claw:** $$1\text{D}6 + 6$$
>
> > **[danger] Wyvern Sting**
> > + **Type:** Ingested
> > + **Delay:** $$1\text{D}2$$ Combat Rounds
> > + **Potency:** 60
> > + **Full Effect:** $$1\text{D}6$$ Hit Point damage, applies –4 penalty to victim's BOD
> > + **Duration:** $$6\text{D}10$$ minutes