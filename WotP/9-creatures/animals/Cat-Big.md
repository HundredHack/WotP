### Cat, Big
> **[danger] Cat, Big**
>
> #### Characteristics:
> **BOD:** $$3D6 + 8 (19)$$ **DEX:** $$3\text{D}6 + 6 (17)$$ **INT:** $$2\text{D}6 + 12 (19)$$ **REA:** $$5$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$15$$ **MW:** $$8$$ **AP:** $$2$$ **MR:** $$34$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}8 + 5$$
>   + **Claw:** $$1\text{D}6 + 5$$