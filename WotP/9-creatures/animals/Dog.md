# Dog
> **[danger] Dog**
>
> #### Characteristics:
> **BOD:** $$2\text{D}6 + 2 (9)$$ **DEX:** $$2\text{D}6 + 6 (13)$$ **INT:** $$2\text{D}6 + 6 (13)$$ **REA:** $$5$$ **CHA:** $$1\text{D}6 + 6 (9)$$
> #### Attributes:
> **HP:** $$9$$ **MW:** $$5$$ **AP:** $$0$$ **MR:** $$26$$
> #### Combat:
> + **Close Combat:** $$40\%$$
>   + **Attack:** $$1\text{D}6 + 2$$