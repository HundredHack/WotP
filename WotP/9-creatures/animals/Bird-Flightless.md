# Bird, Flightless
> **[danger] Bird, Flightless**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 12 (23)$$ **DEX:** $$3\text{D}6 + 6 (17)$$ **INT:** $$2\text{D}6 + 12 (19)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$17$$ **MW:** $$9$$ **AP:** $$3$$ **MR:** $$34$$
> #### Combat:
> + **Close Combat:** $$45\%$$
>   + **Peck:** $$1\text{D}6 + 6$$
>   + **Kick:** $$1\text{D}8 + 6$$