### Beetle, Giant
> **[danger] Beetle, Giant**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 8 (19)$$ **DEX:** $$2\text{D}6 + 6 (13)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$2$$ **CHA:** $$1\text{D}6 + 6 (9)$$
> #### Attributes:
> **HP:** $$14$$ **MW:** $$7$$ **AP:** $$5$$ **MR:** $$13$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}8 + 5$$