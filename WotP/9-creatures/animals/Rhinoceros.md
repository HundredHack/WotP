# Rhinoceros
> **[danger] Rhinoceros**
>
> #### Characteristics:
> **BOD:** $$2\text{D}6 + 14 (21)$$ **DEX:** $$2\text{D}6 (7)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$16$$ **MW:** $$8$$ **AP:** $$5$$ **MR:** $$14$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}6 + 5$$
>   + **Gore:** $$1\text{D} + 5$$
>   + **Trample:** $$1\text{D}12 + 5$$