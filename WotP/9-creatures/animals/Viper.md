# Viper
> **[danger] Viper**
> #### Characteristics:
> **BOD:** $$2\text{D}6 + 2 (6)$$ **DEX:** $$3\text{D}6 + 18 (27)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$9$$ **MW:** $$5$$ **AP:** $$1$$ **MR:** $$27$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}6 + 2 + \text{Venom}$$
>
> > **[danger] Venom, Viper**
> >
> > _The venom of this terrible viper causes severe pain and prevents the blood from coagulating while dissolving muscle tissue at the bite location._
> > + **Delay:** $$1\text{D}4$$ rounds
> > + **Duration:** $$6\text{D}10$$ minutes
> > + **Potency:** $$50\%$$
> > + **Effect:** $$1\text{D}4$$ damage every ten minutes, $$-3$$ BOD