# Animals
_General info about animals_

## Overview

| Animal                                 |   HP   |   MR   |   AP   |   DB   | Combat                                                                                                 |
|---------------------------------------:|:------:|:------:|:------:|:------:|:-------------------------------------------------------------------------------------------------------|
| [Ant, Giant](Ant-Giant.md)             | $$10$$ | $$13$$ | $$5$$  | $$3$$  | $$60\%$$, Bite      $$1\text{D}8$$                                                                     |
| [Bear](Bear.md)                        | $$17$$ | $$11$$ | $$3$$  | $$6$$  | $$60\%$$, Bite      $$1\text{D}8$$,                Claw      $$1\text{D}6$$                            |
| [Beetle, Giant](Beetl-Giant.md)        | $$14$$ | $$13$$ | $$5$$  | $$5$$  | $$50\%$$, Bite      $$1\text{D}8$$                                                                     |
| [Bird, Flightless](Bird-Flightless.md) | $$17$$ | $$34$$ | $$3$$  | $$6$$  | $$45\%$$, Peck      $$1\text{D}8$$,                Kick      $$1\text{D}6$$                            |
| [Cat, Big](Cat-Big.md)                 | $$15$$ | $$34$$ | $$2$$  | $$5$$  | $$60\%$$, Bite      $$1\text{D}6$$,                Claw      $$1\text{D}6$$                            |
| [Cattle](Cattle.md)                    | $$16$$ | $$14$$ | $$2$$  | $$6$$  | $$40\%$$, Charge    $$1\text{D}8$$,                Trample   $$1\text{D}8$$                            |
| [Crab, Giant](Crab-Giant.md)           | $$20$$ | $$7$$  | $$6$$  | $$7$$  | $$50\%$$, Claw      $$1\text{D}10$$                                                                    |
| [Crocodile, Giant](Crocodile-Giant.md) | $$17$$ | $$11$$ | $$5$$  | $$6$$  | $$50\%$$, Bite      $$1\text{D}8$$                                                                     |
| [Dog](Dog.md)                          | $$9$$  | $$26$$ | $$0$$  | $$2$$  | $$40\%$$, Bite      $$1\text{D}6$$                                                                     |
| [Elephant](Elephant.md)                | $$27$$ | $$11$$ | $$3$$  | $$10$$ | $$45\%$$, Trample   $$1\text{D}12$$,               Tusk      $$1\text{D}10$$, Trunk Grapple            |
| [Hawk, Giant](Hawk-Giant.md)           | $$24$$ | $$18$$ | $$3$$  | $$9$$  | $$80\%$$, Claw      $$1\text{D}8$$,                Bite      $$1\text{D}6$$                            |
| [Hawk](Hawk.md)                        | $$5$$  | $$29$$ | $$0$$  | $$1$$  | $$50\%$$, Claw      $$1\text{D}6$$,                Bite      $$1\text{D}4$$                            |
| [Horse](Horse.md)                      | $$17$$ | $$38$$ | $$2$$  | $$5$$  | $$40\%$$, Kick      $$1\text{D}6$$                                                                     |
| [Lizard, Giant](Lizard-Giant.md)       | $$13$$ | $$19$$ | $$2$$  | $$4$$  | $$25\%$$, Bite      $$1\text{D}6$$,                Kick      $$1\text{D}8$$                            |
| [Octopus, Giant](Octopus-Giant.md)     | $$23$$ | $$23$$ | $$4$$  | $$9$$  | $$50\%$$, Bite      $$1\text{D}8$$,                Arm       $$1\text{D}4$$                            |
| [Pteranodon](Pteranodon.md)            | $$15$$ | $$19$$ | $$3$$  | $$5$$  | $$50\%$$, Bite      $$1\text{D}8$$,                Claw      $$1\text{D}6$$                            |
| [Python, Giant](Python-Giant.md)       | $$15$$ | $$13$$ | $$3$$  | $$5$$  | $$50\%$$, Bite      $$1\text{D}4$$,                Constrict $$1\text{D}8$$                            |
| [Rhinoceros](Rhinoceros.md)            | $$16$$ | $$14$$ | $$5$$  | $$5$$  | $$50\%$$, Bite      $$1\text{D}6$$,                Gore      $$1\text{D}8$$,   Trample $$1\text{D}12$$ |
| [Triceratops](Triceratops.md)          | $$26$$ | $$20$$ | $$10$$ | $$10$$ | $$50\%$$, Tail Lash $$1\text{D}12$$,               Gore      $$1\text{D}10$$                           |
| [Tyrannosaurus](Tyrannosaurus.md)      | $$30$$ | $$10$$ | $$10$$ | $$12$$ | $$60\%$$, Bite      $$1\text{D}10$$,               Stomp     $$1\text{D}10$$                           |
| [Velociraptor](Velociraptor.md)        | $$15$$ | $$28$$ | $$5$$  | $$5$$  | $$50\%$$, Bite      $$1\text{D}8$$,                Claw      $$1\text{D}6$$,   Foreclaw $$1\text{D}4$$ |
| [Viper](Viper.md)                      | $$9$$  | $$27$$ | $$1$$  | $$2$$  | $$60\%$$, Bite      $$1\text{D}4 + \text{Venom}$$                                                      |
| [Wolf](Wolf.md)                        | $$12$$ | $$26$$ | $$1$$  | $$3$$  | $$50\%$$, Bite      $$1\text{D}6$$,                Claw      $$1\text{D}6$$                            |