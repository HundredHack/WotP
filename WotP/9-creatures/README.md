# Creatures
In Tephra, there are two broad categories of creatures: Animals and Beasts.

Animals are, as you might expect, just that - normal animals of the world.
Though, in the Winter of the Phoenix, even simple animals may be changed by thte magic of the world.

Beasts are more thoroughly recognizable as having been altered by the magic of the world, transforming into shapes and having abilities beyond the normal realm.

In both cases the statistics listed should be considered average starting points for the creatures.
Creatures can be improved with improvement points, just like characters - improving their characteristics, skills, and granting them special abilities, especially knacks.

> **[warning] GM Tip:**
>
> Do not increase encounter difficulty by increasing numbers of creatures.
> A much better way is to increase the power of individual creatures, by increasing skills and magic use, to be closer to the player character power level.
> WotP combat works best when there is roughly the same amount of creatures as player characters.