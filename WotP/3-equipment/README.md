# Equipment

Fantasy roleplaying games can be thought of as a form of cooperative improvised theatre.
You could think of the players as the actors and the Games Master as the director and production team providing the stage and scenery, a huge big budget supporting cast and every prop that the actors could possibly need.
This chapter deals with the props, the equipment that the player characters will be using.

> **[info] In-Game Economics**
>
> These rules do not give detailed rules for trading and fantasy world economics.
> Although dry economic markets are unlikely to feature heavily in adventure stories, the exploits of daring and wily merchant adventurers are.
> The following section outlines how to approach such stories in WotP.

## Opportunities for Merchant-Based Games

Some players will feel inclined to create colourful and flamboyant Merchant characters and weave stories around their trade missions to far off unexplored countries creating drama and tension on their trade negotiations and deals.
This is great and is to be encouraged.
Opposed Trade tests can be used to handle the outcome of such action where it is less than clear cut, and the ebb and flow of the character's finances acts as an indicator of success (see the [Trade skill description][trade-skill]).
The more martially and magically inclined characters can provide support and have their moments in the spotlight too on these mercantile adventures, taking on the villains hired by their rivals in commerce.
If you are in need of inspiration then you only have to look to the real life historical adventures of Marco Polo.

Merchant characters also make great information gatherers, since they tend to have good social skills.
Often this goes on under the cover of trading in the market, gathering gossip from the locals, or sorting out a new trade deal with a noble family, which is a legitimate way of finding information about a noble.

## Availability of Goods

The equipment lists serve as 'game tools' to allow players to quickly and easily buy equipment for their characters.
The range of goods listed at the quoted prices is only going to be available in a large metropolis with organised markets and districts given over to shops and mercantile activity.
In less prosperous cities and towns there is a smaller range available, sometimes at higher costs.
In rural areas, only local produce and a small amount of locally crafted goods can be bought at a reasonable price.
There might be oddities to this model and these can lead to further adventure.

## Barter
Coins are the main exchange method for the landed nobility and rich merchants.
Barter is the main method of exchanging goods for people outside of the main urban areas.
In such transactions successful Opposed Test using the Trade of the buyer versus the Trade of the seller are needed for the buyer to get the best deal. 

## Consequences 
The main thing to remember is that with any item of equipment there are consequences in their use as well as benefits.
The most obvious consequence is encumbrance.
A heavily armoured and equipped character will be slowed, unable to use skills as effectively and will become fatigued more easily. 

A less obvious effect is that an obviously well equipped character becomes a target for both minor and major theft.
From the opportunistic thief who desires the player's new sword to the more organised bandit group who targets the party because they believe that they have a stash of treasure back at their base because of all the flashy new equipment they are wearing. 

There might also be social consequences.
In civilised towns and cities, prominent displays of arms and armour may unsettle and upset the locals and bring about the unwanted attention of the Watch who want to make sure that the characters are not violent troublemakers.
In some more draconian fantasy lands there may even be laws and social codes that dictate what arms and armour a citizen may own and in what situations they may carry it.   

## Currency  

Coins are usually created in ‘mints' tightly controlled by the local nobility, appointed by the local ruler, whose head appears on one side of the coin.
Other sources of coin are usually the treasure troves of monsters, whose assets are brought into the economy by enterprising adventurers. 

Currency can be based upon whatever is valued by the culture using it.
Being a fantasy game, many variant systems of currency can be created.
For example, a system that uses the teeth of slain dragons or magical gemstones enchanted with minor magic that is useful in everyday life can be used as an exchange mechanism. 

For ease of use here's a simple coin based currency that will be used throughout the rest of this book to give value to an item. 

+  5 Lead Bits (LB) = 1 Copper Penny (CP)  
+ 10 Copper Pennies (CP) = 1 Silver Piece (SP) 
+ 20 Silver Pieces (SP) = 1 gold ducat (GD) 