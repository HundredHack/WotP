# Organizations

The following is a non-exhaustive list of organizations available in Tephra.
Some of them are more generic, templates for the sorts of organizations you might find anywhere.
Others are specific and more flavorful, such as the Griffin Knights.

## Griffin Knights
## The Merchant Council of Crantz
## The Duchy of Stern
## The Great White Kingdom
## Yanan Isik
## Shona's Daughters
## Kostian School
## Sweetwater Pirate Crew
## Farmer's Association
## Explorer's Guild
## Miner's Guild
## Fishery Union
## Death Cult
## Hunter
## Imperial College
## Keepers of the World
