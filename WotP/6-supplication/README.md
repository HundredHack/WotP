# Supplication
Members of organizations who perform miracles in the name of those organizations are known as Supplicants.
Supplication is the act of binding oneself to the will of an organization in a ceremony that establishes a magical link between the supplicant and one or more Grantors.

The ceremony is, itself, a dweomer of the Craft, a magical binding contract.
The Supplication dweomer requires that the Supplicant truly, honestly want to use power to further the goals of the organization they are supplicating to.
This prevents the Supplicant from performing miracles against the organization.
The supplication dweomer sends information back to the Grantor(s), allowing them to know why the Supplicant chooses to perform any given miracle.
Finally, the dweomer allows the Supplicant to communicate with the Grantors and receive messages from them in turn.

## Organizations
Any organization with enough power can employ or attact Crafters who can apply the Supplication dweomer.
The organization might be religious, civil, professional, social, or other - but all organizations are defined as sharing some core purpose.
Organizations are described using the following format:

> **[warning] Name of Organization**
>
> _Short description which briefly covers the organizations purpose, core beliefs, and place in the world._
>
> + **Type:** This is the type and size of organization.
>   State organizations include Empires, Confederations, Kingdoms, Cities, Tribes, etc.
>   Religious organizations include Great Deities, Major Deities, Minor Deities, and Hero Cults.
>   Other organizations include Global Guilds, National Guilds, Local Groups, Unions, Clubs, etc.
> + **Members:** The type of people who are typically part of the organization.
> + **Duties:** This is what the organization expects of its members.
>   Break these rules and expect expulsion.
>   On the other hand, follow these rules and promote them to others and the character will advance in the organization's hierarchy.
> + **Skills:** These are skills favored by the organization and taught to its members by Grantors and Supplicants alike.
> + **Knacks:** Sharing the core beliefs of the organization and living up to them often means that members acquire certain knacks over time.
> + **Miracles:** These are the incredible powers that a Supplicant of the organization can wield on behalf of the organization.
> + **Special Benefits:** Any bonuses to skill use or other special abilities or advantages that a Supplicant gains by being a member of the organization.

### Improvement Points and Supplicant Duties
Each organization has a set of Duties which represent the organization's objectives in the world.

When a character does an action that fulfills one of the Organizational Duties they gain one Improvement Point for a minor act and up to three points for a major act.

When a character does an action that goes against one of the Organization Duties they lose between one and three Improvement Points depending on the grievousness of their transgression.
If they have no Improvement Points left then they start to lose Miracles and knacks gained from the organization.
The player may chose which miracle to lose and, if no miracles remain, they begin to lose knacks instead.

If the offending character has no Improvement Points, miracles, or knacks to lose then they are excommunicated from the organization and may never join it again.

### Organization Ranks
There are five ranks of organizational membership: lay members, Supplicants, Champions, Principals, and Grantors.

#### Lay Members
Lay members are normal members of the organization.
They regularly attend important organizational events and do their best to uphold the strictures of the organization.
In return, the organization protects them as best it can, and its Supplicants and Principals perform miracles on their behalf.
Lay members cannot perform Miracles, although they may acquire low Magnitude (no more than Magnitude 2) organizational knacks.
To become a lay member of an organization a character must have Relationship (Organization) of at least 20%.

#### Supplicants
Supplicants are members who have dedicated their lives to the tenents of the organization and have undergone the Supplication dweomer.
They always attend important organizational events and always uphold the strictures of the organization.
In return, the organization will pay ransoms if they are captured and allow the Supplicant to learn to perform Miracles.
Supplicants can acquire unlimited Magnitude of any Organizational Knack and up to 2 Magnitude of any Miracle available to the Organization.
To become a Supplicant, a member of an Organization must have a Relationship (Organization) of at least 40% and pay an Improvement Point cost of two points.
This will immediately give the character a Supplication (Organization) skill equal to their Relationship (Organization) skill.

#### Principals
Principals are the living embodiment of the organizational, instructed by their organization to be its living representative in the world.
They lead the events for their organization.
In return, the organization will pay ransoms if they are captured and grant them the inner secrets of their organization (this means all available Miracles at unlimited Magnitude).
To become a Principal a character must have a Supplicant (organization) and two of the organizational skills at least 75%, there must be a vacancy in the organization's hierarchy, or the Principal be willing to become a missionary and establish a new chapter.
In addition the Player must pay five Improvement Points.

**Living Miracle:** Upon becoming a Principal the character gains a powerful gift from the organization.
This gift is a combination of the changes in the character over the course of becoming a principal _and_ the power that is placed in them on behalf of the organization.
The character immediately gains 6 magnitude worth of Organizational Knacks and 3 magnitude worth of Miracles and their INT and CHA both increase by three.
However, if the Principal ever takes an action that offends the organization, they lose twice as many Improvement Points and cannot gain new Improvement Points from the organization until they've fulfilled a Quest of Repentence which directly benefits the organization.

#### Champions
These are Supplicants who have become empowered warriors who protect the locations and members of their Organization.
Not all organizations have Champions, especially those dedicated to non-violent goals, but where they do, these warriors ceaselessly crusade to protect the devoted and punish the organization's enemies.

The minimum requirement to become a Champion is to have Supplicant (Organization) of at least 50% and a Close Combat or Ranged Combat skill of 75%.
In addition, the Player must pay five Improvement Points.

**Miraculous Panoply:** When someone becomes a CHampion they are gifted a specially consecrated weapon that gives them a bonus when wielded on behalf of the organization.
The bonus is usually +25% to the appropriate combat skill and double damage when fighting for the organization.
They also gain armor which is magically imbued by the Organization's Grantors.
Normally, this is at least double the normal AP of the armor type used and it may have additional powers depending on the Organization.

**Warrior Miracles:** Champions may acquire Miracles that further enhance their ability to fight on the behalf of the organization at unlimited Magnitude.

**Ransom:** CHampions are incredibly useful to the organization they belong to, which will always pay any ransom or make a rescue atempt when a Champion is captured.

**Responsibilities:** Like Principals, Champions are expected to uphold the member duties of the organization.
Also, as the organization's warriors they are expected to take up arms against any aggressor who attacks its members or the organization's locations.

#### Grantors
Grantors are powerful Crafters who are dedicated to the organization.
Instead of wielding power on behalf of the Organization, as Supplicants do, Grantors are the people responsible for pooling power, applying the Supplication dweomer, and, if the organization has Champions, for imbuing their panoply.

Grantors, like all members of the organization, must uphold the organizational duties.
Additionally, they are responsible for interacting with Supplicants, Principals, and Champions, spending most of their time and power in locations run by Organizations and investing power into their Supplicants and punishing them for crossing the Organization.

To become a Grantor, a character must have Relationship (Organization) of 50% or higher and a Craft skill of 75% or higher.
Additionally, they must invest two Improvement POints.

In return, Grantors treat Relationship (Organization) tests as Easy and can direct Supplicants to perform actions on behalf of the organization.
There is usually no distinct hierarchy of Grantors, their place in the Organization being more or less related to how much they do for the organization by providing power to Supplicants and selecting goals for them that further the organization.

> **[info] Principals and Champions, What Do They Do?**
>
> Principals and Champions don't just hang around their organizations doing their duties.
> They have plenty of Supplicants and lay worshippers to do the more mundane administrative tasks, such as collecting tithes and feeding the poor.
> As player characters, their lives are more interesting and the source of constant Questing on behalf of their organization.
> Some of the Quests that they might get involved in are as follows:
>
> + Going out and converting new members.
> + Actively fighting the enemies of the organization.
> + Recovering long-lost symbols and powerful artefacts of the organization.
> + Attending a cross-organization meeting to deal with all the politics and misunderstanding to come to a consensus about what to do about a common enemy.
> + Rushing to the aid of an embattled and besieged town of his faithful members beset by enemies or some other form of peril.
> + Visiting the hinterlands to provide guidance and duties to those in need
> + Traveling to a distant Holy Mountain to commune directly with their Deity or otherwise performing idealistic inspirational acts, or to prove their worth.
> + Going on a special mission where success depends on Miracles.
> + Traveling as a special envoy of the organization to show due deference to the King / Guild Master / High Emperor.

> **[success] Summary of Organization Ranks
>
> | Rank     | Minimum Skill                    | IP Cost | Benefits |
|:----------:|:--------------------------------:|:-------:|:---------|
| Lay        | Relationship 20%                 | 0       | Acquire Organizational Knacks Magnitude 1-2
| Supplicant | Relationship 40%                 | 2       | Acquire Organizational Knacks Unlimited Magnitude, Miracles Magnitude 1-2
| Principal  | Supplication 75%                 | 5       | Acquire Organizational Knacks and Miracles at Unlimited Magnitude, +3 INT/CHA
| Champion   | Supplication 50%, Any Combat 75% | 5       | Acquire Organizational Knacks Unlimited Magnitude, special Miracles, gain miraculous panoply
| Grantor    | Relationship 50%, Craft 75%      | 2       | Acquire Organizational Knacks Unlimited Magnitude, Easy Relationship Tests

## Intervention

As well as having miracles, and allies in the organization that teach them, supplicants also gain the ability to call for Intervention.

A character who is a Supplicant, Principal, or Champion can call upon his Grantor for Intervention whenever he faces a desperate situation.
He may even do so if dead or unconscious, as long as it is called for in the instant that consciousness fades or death occurs.

When Intervention is requested, roll $$1\text{D}100$$.
If this roll is equal to or less than the character's CHA, the call for aid is answered.
However, grantors demand a heavy price for their help and the character will suffer a permanent loss of Supplication skill equal to the $1\text{D}100$$ roll, if they are successful.
If the character's Suplication skill is reduced to 0 by this, they are immediately excommunicated from the organization and lose all miracles and other advantages of being a supplicant of the organization.

A character can only call for Intervention once per month, whether he is successful or not.

Intervention can take many forms and the following can be considered guidelines:

+ Allow the character to perform any organizational miracle at any Magnitude..
+ Allow a miracle to affect every member of the organization within the character's line of sight.

Various other effects are also possible dependant on the nature of the character's organization.
Examples could include creating a fog to allow the character to escape from enemies, doubling their STR to defeat a powerful enemy or causing a river to break its banks to stall an invading army.