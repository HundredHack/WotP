# Close Combat
## Combat Actions
+ **Charge:**  If a character can move a minimum of five yards towards his opponent, then he can make a charge.
  He  may  move  a  distance  up  to - but no more than - twice his  Movement Rate.
  This must be in a straight line and he must end up adjacent to an enemy.
  When the move is complete, a close combat attack may be made against the enemy.
  If the attack is successful, the character gains a bonus of $$+1\text{D}6$$ damage.
  He loses his defensive reaction for the round that he charges on.
  Characters may not charge uphill and gain the damage bonus. 
+ **Close Combat Attack:**  The character can make a single close combat attack.
  As well as a normal attack, there are the following special attacks. 
  + **All out  Attack:** The attacker gives up their Reaction for the round but gains a second attack, which happens straight after the first attack.
    Both attacks are at -25% due to the loss of skill during this frenzied attack.
    This type of attack cannot be combined with Great Attack or Disarming Attack. 
  + **Disarming Attack:**  Attacker attacks at -25% to his weapon skill with the aim of disarming their opponent either of their weapon or shield.
    If the attack is successful and the opponent fails to parry or dodge, the weapon or shield is thrown $$1\text{D}6$$ yards away from the owner.  
  + **Great Attack:**  This attack is made using swords, axes or maces where the attacker has enough room to wind up the weapon for a really forceful blow.
    The attacker gains a +25% to attack and automatically does maximum damage but loses his reaction for that combat round. 
  + **Natural Weapon attack:** Using the characters's natural weapons be it claw, bite, kick or fist. 
  + **Grapple Attack:** The attacker attempts to grab an opponent and an opposed Close Combat Attack is made.
    If the attacker wins they may choose to inflict pain, immobilise or throw their opponent.
+ **Grappling:** Once one side of a fight has started a grapple the grappling combatants suffer a $$-25\%$$ penalty to any tests that do not target or directly respond to their grapple partner.
  Grappling combatants may not use Reactions and are limited to only the following special Combat Actions:
  + **Break Free:** To break out of a grapple, the character makes an opposed Grapple Attack.
    If the character succeeds his roll while his opponent fails then the character has succeeded in breaking free and the combatants are no longer grappling, though they will be adjacent.  
  + **Immobiliise:** While immobilized enemies are considered helpless.
    Once per round the defender may attempt to break free.
  + **Inflict Pain:** The grappler inflicts damage of $$1\text{D}4 + \text{damage modifier}$$, ignoring AP.
    Once per round the defender may attempt to break free or may attempt to turn the tables on their attacker by counter grappling.
  + **Throw:** The opponent is thrown 2 yards and suffers $$1\text{D}4$$ damage, ignoring AP, and the grapple ends.
+ **Ranged Combat Attack:**  The character can make a single ranged combat attack.
  As well as a normal attack, there is the following special attacks. 
  + **Aim:** Every round spent aiming adds a $$+25\%$$ bonus to the character's Ranged Combat skill.
  This bonus only applies to the first attack the character makes with the weapon, which must be fired at the target being aimed at.
  A character can take no other Reaction while aiming without losing the aim bonus.  
+ **Intimidate/Persuade:**  The character tries to get the other side to surrender or flee.
  This can either be targeted at a single enemy or a group.
  Do an Opposed roll using the character's Influence vs. the enemies' Persistence, modified as listed below. Groups roll once using the Persistence of the group leader.
  If the group leader's Influence skill is higher than his Persistence, then they may use that skill instead.
  Apply the following modifiers to the enemy's skill depending on the state of the enemy. 
  + $$+50\%$$ if the enemy is still at full strength, or has only taken some minor wounds. 
  + $$+25\%$$ if the enemy out numbers the player's side, but have had at least 25% losses either in numbers or hit points. 
  + $$-25\%$$ if the enemy is fewer than the player's side and has taken some wounds. 
  + $$-50\%$$ if the enemy has taken more than half hit points in wounds and/or has seen half his group incapacitated by the players. 
  + **Note:** these modifiers are not cumulative. Apply the one that best describes the situation. 
    If the enemy is at full strength and/or outnumbers the player characters then only a critical roll for Influence vs a failed Persistence roll will make them surrender.
    A fumbled Persistence roll will see the enemy suddenly rout.

    When the player is attempting the roll they must declare whether they are targeting the whole group or singling out an individual.
    The GM has the final say on who is targeted and if if attempt is possible at all.  
+ **Set Weapon:**  A character can spend their Action setting the shaft of a weapon, such as a spear or polearm, in the ground in anticipation of a charge from an opponent.
  When the charge actually comes the character automatically gets an attack at $$+25\%$$ before the charging character gets their attack.
  If the character makes any other action or reaction before the charge, the weapon becomes ‘unset'.
+ **Cast Spell:** Spells take effect when they are cast.
+ **Exercise a Knack:** Knacks that have to be exercised are a combat action.
+ **Perform a Miracle:** Miracles take effect when they are performed.
+ **Ready Weapon:**  Drawing a sword from its sheath, unhooking an axe from one's belt, nocking an arrow to one's bow – all these actions take one combat round.
  Ranged weapons can be reloaded with this action – this takes as many Combat Rounds as noted in the weapon's description.  
+ **Skill Use:** The character performs one action which requires the use of a skill, such as opening a locked door with the Mechanisms skill.

## Movement Actions
A character may make one of the following Move Actions for Free. and unless noted otherwise does not lose either their Action or Reaction.
+ **Change Stance:**  The character may stand up from prone, or vice versa. 
+ **Fighting Retreat:**  A character may move their full Movement directly away from an enemy he is fighting with a $$25\%$$ bonus to defensive reactions.
+ **Standard Move:** The character may move a distance up to his Movement Rate. 
+ **Sprint:** The character may move a distance up to twice their Movement Rate, forsaking their attack and only being able to dodge as defensive reaction.  

## Defensive Reactions
A character can make one Reaction in a combat round. Unlike Combat Actions, Reactions do not have to specify a target but can be triggered by any given attack.
They do, however, have to be declared.

There are two types of Reaction – dodge and parry.  

Parries can be made against close combat attacks, and, with a shield, ranged attacks.
If a character successfully parries an unarmed attack with a weapon, inflict the weapon's damage on the attacker.
In any case, a successful parry negates the damage of the attack if the size of the parrying weapon or shield is the same size category or larger than the attacker's weapon.
If the defender's weapon or shield is one size smaller it only reduces the damage by half.
If the defender's weapon or shield is two or more sizes smaller than the attacker's weapon the parry is ineffective.

Dodges can only be made against attacks and (some) spells providing the target is aware of the attack and, if successful, negate the effects of the attack / spell (since it doesn't land).

## Making Close Combat Attacks
1.  **Making the Attack:** To attack, the player simply rolls $$1\text{D}100$$ and compares it to the character's Close Combat skill which may be modified for the specific situation or special attack being attempted. 
  If a character rolls equal to or lower than his Weapon skill, he has hit his target.  
  If a character rolls greater than his Weapon skill, he has missed his target.  
2.  **Target Reaction:** If the enemy has already reacted this round, or chooses not to React against this attack, then this attack is unopposed.
  Move straight on to Damage Resolution.   
  If the attack is opposed, the defender makes a Dodge or Parry (see below). 
3. **Damage Resolution:** If the attack is successful, damage is rolled.
  Each weapon has its own Damage score, to which is added the attacker's Damage Modifier in order to determine the total damage being dealt.  
  If the defender is armoured then the armour will absorb some of this damage. Reduce the attack's damage by the armour points (AP) of the defender's armour.  
4. **Damage Application:**  Apply any remaining damage to the defender's hit points.

> **[info] Combat Results**
>
> | Attacker | Defender's Reaction | Result |
|:----------:|:-------------------:|:------:|
| Fumble     | No need to roll     | Attacker fumbles.
| Failure    | No need to roll     | Attacker fails to hit defender.
| Success    | Fumble              | Attacker hits, defender takes damage rolled minus AP and fumbles.
| Success    | Failure             | Attacker hits, defender takes damage rolled minus AP.
| Success    | Success             | If dodging, defender avoids; if parrying, damage reduced based on size.
| Success    | Critical            | Defender avoids attack and takes no damage; ignore size for parrying weapon/shield.
| Critical   | Fumble              | Attacker does maximum damage and ignores defender's AP. Defender fumbles.
| Critical   | Failure             | Attacker does maximum damage and ignores defender's AP.
| Critical   | Success             | Attacker does maximum damage and ignores defender's AP.
| Critical   | Critical            | Attacker hits, defender takes damage rolled minus AP.