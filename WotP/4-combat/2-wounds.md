# Damage and Wounds
When a character successfully scores damage against a target it must be deducted from the target's hit points.
Every weapon has a damage rating, which is listed in its statistical entry in the relevant weapon table in the Equipment chapter.
This rating is the amount of dice rolled when the weapon successfully hits a target.
The attacker's Damage Modifier is usually added to this.

All damage is taken away from Hit Points.

### Last hit point.
When Hit Points are reduced to the final one the character falls prone and must make an immediate Resilience test to stay conscious.

### Hit points equal zero.
The character is dead.
In the grim and gritty world of Tephra combat there is no chance to make farewell speeches.
You can spend Hero Points however to avoid death.

### Major Wounds
If the character takes half of their original Hit Points in one go then they suffer a major wound.
This represents badly mangled limbs, shattered bones and severely damaged internal organs.
Roll on the Major Wound Table below to see what type of wound the character has suffered.
They must immediately make a hard Resilience roll (no modifiers or assistance will affect the test), or fall unconscious.
If the test is sucessfull then the character may only fight on for as many combat rounds as their remaining hit points before failing unconscious; further, all initiative tests they make for the remainder of the combat are hard tests.
This is in addition to any effects described below.
The effects of major wounds are permanent, unless healed magically.

> **[success] Major Wound Table**
>
> | Result on $$1\text{D}10$$ | Major Wound |
|:----------------:|:-----------:|
| $$1$$            | Lose an eye, all visual Perception tests are now hard, lose $$4$$ points of DEX, $$1$$ point of CHA permanently.
| $$2$$            | Cracked skull, brain damage. Lose $$4$$ points of INT. All skills involving mental processes become $$-25$$ permanently. This includes Perception, Persistence, Craft, and all Lore , Culture, and Artisanry skills.
| $$3$$            | Left Leg muscles badly cut/mangled or leg bone shattered and becomes useless. Fall prone, can only crawl at $$1\text{yd}$$ per round. Lose $$2$$ points of DEX and $$2$$ points of BOD permanently.
| $$4$$            | Left Leg muscles badly cut/mangled or leg bone shattered and becomes useless. Fall prone, can only crawl at $$1\text{yd}$$ per round. Lose $$2$$ points of DEX and $$2$$ points of BOD permanently.
| $$5$$            | Broken ribs, all skills are hard, due to severe pain.
| $$6$$            | Slashed stomach. Lose one extra hit point per round from blood loss. Lose $$3$$ points of BOD permanently.
| $$7$$            | Heart stops in shock! Lose consciousness for the next $$\text{D}10$$ combat rounds, falls prone and can not move. Lose $$2$$ points of BOD permanently.
| $$8$$            | Spine broken. The character permanently paralysed below the neck (odd result on dice) or the torso (even result on dice). Half DEX permanently.
| $$9$$            | Left Arm badly broken and becomes useless permanently. Automatically drop any held items.
| $$10$$           | Right arm badly broken and becomes useless permanently. Automatically drop any held items.

### Fatal Wounds
Character takes damage equal to, or in excess of, original hit points in one go.
This represents hacked off limbs, blows that shatter rib cages, decapitation, blows that stab the heart or other other vital organs directly.
The character is immediately dead.